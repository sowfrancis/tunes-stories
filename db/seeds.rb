# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)


# Create Eras
years = (1900..2023).to_a

years.each do |year|
	Fabricate(:era, year: year.to_s)
end

# Create default genres

genres = ['Hip hop', 'Jazz', 'Soul', 'Rock', 'Funk']

genres.each do |genre|
	Fabricate(:genre, name: genre)
end

# Create artits

artists = [
	 'kendrick lamar',
	 'J cole',
	 'J dilla',
	 'Busta rhymes',
	 'westside gunn',
	 'Mf Doom',
	 'Eminem',
	 'Séu Jorge'
]

if Rails.env.developement?
	artists.each do |name|
		Fabricate(:artist, name:)
	end

	Create stories
	users = Fabricate.times(5, :user)
	users_ids = users.pluck(:id)
	eras_ids = Era.pluck(:id)
	genre_ids = Genre.pluck(:id)


	Artist.all.each do |artist|
		Fabricate(:story, user_id: User.find(users_ids.sample).id, era_id: Era.find(eras_ids.sample).id,
			                 genre_ids: [Genre.find(genre_ids.sample).id],
		                  artist_id: artist.id)
	end
end
