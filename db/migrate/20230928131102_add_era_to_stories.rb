class AddEraToStories < ActiveRecord::Migration[7.0]
  def change
    add_reference :stories, :era, null: false, foreign_key: true
  end
end
