class CreateArtists < ActiveRecord::Migration[7.0]
  def change
    create_table :artists do |t|
      t.string :name
      t.boolean :verified_artist

      t.timestamps
    end
  end
end
