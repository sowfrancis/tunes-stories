class AddArtistNameToStories < ActiveRecord::Migration[7.0]
  def change
    add_column :stories, :artist_name, :string
  end
end
