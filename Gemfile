# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.1.4'

gem 'bootsnap', require: false
gem 'bootstrap'
gem 'byebug'
gem 'devise', '~> 4.9'
gem 'erb2haml', '~> 0.1.5'
gem 'fabrication'
gem 'haml-rails', '~> 2.1'
gem 'image_processing', '~> 1.2'
gem 'importmap-rails'
gem 'jbuilder'
gem 'jquery-rails'
gem 'puma', '~> 5.0'
gem 'rails', '~> 7.0.5'
gem 'rails-controller-testing'
gem 'rubocop', '~> 1.56'
gem 'sassc-rails'
gem "select2-rails"
gem 'simple_form'
gem 'sprockets-rails'
gem 'sqlite3', '~> 1.4'
gem 'stimulus-rails'
gem 'tinymce-rails'
gem 'turbo-rails'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'view_component'
gem "hinter", git: "https://github.com/Oxyless/hinter.git"

group :development, :test do
  # See https://guides.rubyonrails.org/debugging_rails_applications.html#debugging-with-the-debug-gem
  gem 'debug', platforms: %i[mri mingw x64_mingw]
  gem 'ffaker'
  gem 'rspec-rails', '~> 6.0.0'
  gem 'rubocop-rspec', require: false
  gem 'shoulda-matchers', '~> 5.0'
end

group :development do
  gem 'web-console'
end

group :test do
  # Use system testing [https://guides.rubyonrails.org/testing.html#system-testing]
  gem 'capybara'
  gem 'selenium-webdriver'
  gem 'webdrivers'
end

gem 'pg', '~> 1.5'
