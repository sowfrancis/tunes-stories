require 'rails_helper'

RSpec.describe "Comments", type: :request do
  describe "POST /create" do
    let(:user) { Fabricate(:user) }
    let(:era) { Fabricate(:era, year: '1990') }
    let(:genre) { Fabricate(:genre, name: 'hip hop') }
    let(:artist) { Fabricate(:artist, name: 'test') }
    let(:story) { Fabricate(:story, user_id: user.id, era_id: era.id, genre_ids: [genre.id], artist:) }
    let(:comment) { Fabricate(:comment, body: 'test', user_id: user.id, story_id: story.id) }

    before do
      sign_in user
    end

    it 'shoud create a comment' do
      expect do
        post "/stories/#{story.id}/comments", params: { comment: { body: 'test', user_id: user.id, story_id: story.id } }
      end.to change(Comment, :count).by(1)
    end
  end
end
