require 'rails_helper'

RSpec.describe "Users", type: :request do
  describe "GET /show" do
    let(:user) { Fabricate(:user) }

    it 'render template show' do
      get "/users/#{user.id}"

      expect(response).to render_template(:show)
    end
  end
end
