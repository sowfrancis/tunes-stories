require 'rails_helper'

RSpec.describe "Artists", type: :request do
  describe "GET /SHOW" do
    let(:user) { Fabricate(:user) }
    let(:era) { Fabricate(:era) }
    let(:genre) { Fabricate(:genre, name: 'hip hop') }
    let(:artist) { Fabricate(:artist, name: 'test') }
    let!(:stories) { Fabricate.times(5, :story, user_id: user.id, era_id: era.id, genre_ids: [genre.id], artist:) }

    it 'displayed all stories' do
      get "/artists/#{artist.id}"

      expect(assigns(:stories)).to eq stories
    end
  end
end
