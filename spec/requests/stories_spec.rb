require 'rails_helper'

RSpec.describe "Stories", type: :request do
  describe "GET /index" do
    let(:user) { Fabricate(:user) }
    let(:era) { Fabricate(:era, year: '1990') }
    let(:era_two) { Fabricate(:era, year: '2000') }
    let(:era_three) { Fabricate(:era, year: '2013') }
    let(:genre) { Fabricate(:genre, name: 'hip hop') }
    let(:artist) { Fabricate(:artist, name: 'test') }
    let!(:stories) { Fabricate.times(2, :story, user_id: user.id, era_id: era.id, genre_ids: [genre.id], artist:) }

    context 'when all stories' do
      it 'displayed all stories' do
        get '/stories'
        
        expect(assigns(:stories)).to eq stories.reverse
      end
    end

    context 'when params search by era' do
      it 'gets all stories by era' do
        get '/stories', params: { q: { era: '1990', genres: '', queries: '' } }
        
        expect(assigns(:stories).first.era.year).to eq '1990'
      end
    end

    context 'when params search by genres' do
      let(:genre_two) { Fabricate(:genre, name: 'acid jazz') }
      let(:story) { Fabricate(:story, era_id: era.id, user_id: user.id, artist:, genres: [genre_two]) }

      it 'gets all stories by genres' do
        story
        
        get '/stories', params: { q: { genres: 'Acid Jazz', era: '', queries: '' } }
        
        expect(assigns(:stories).count).to eq 1
      end
    end

    context 'when params search by title or artist name' do
      let(:genre_two) { Fabricate(:genre, name: 'Jazz') }
      let(:artist_two) { Fabricate(:artist, name: 'J dilla') }
      let(:story) { Fabricate(:story, title: 'Damn', era_id: era.id, user_id: user.id, artist: artist_two, genre_ids: [genre_two.id]) }

      it 'gets all stories by artist name' do
        story

        get '/stories', params: { q: { queries: 'J dilla', genres: '', era: '' } }
        
        expect(assigns(:stories).first.artist.name).to eq 'j dilla'
      end

      it 'gets all stories by story title' do
        story

        get '/stories', params: { q: { queries: 'Damn' } }

        expect(assigns(:stories).first.title).to eq 'damn'
      end
    end
  end

  describe "POST /create" do
    let(:user){ Fabricate(:user) }
    let(:era) { Fabricate(:era) }
    let(:genre) { Fabricate(:genre, name: 'hip hop') }

    before do
      sign_in user
    end

    context 'when is a success' do
      it 'create a story' do
        expect do 
          post '/stories', params: { story: 
            { title: 'titre', body: 'Yeah', user_id: user.id, era_id: era.id, genre_ids: [genre.id], artist_attributes: { name: 'test' } } }  
        end.to change(Story, :count).by(1)
      end
    end

    context 'when is a failure' do
      it 'does not create a story' do
        post '/stories', params: { story: { title: '', body: 'Yeah', user_id: user.id, era_id: era.id } }

        expect(response).to render_template(:new)     
      end
    end
  end

  describe "PUT /update" do
    let(:user){ Fabricate(:user) }
    let(:user_two){ Fabricate(:user) }
    let(:era) { Fabricate(:era, year: '2000') }
    let(:genre) { Fabricate(:genre, name: 'jazz') }
    let(:artist) { Fabricate(:artist) }
    let(:story) { Fabricate(:story, user_id: user.id, era_id: era.id, genre_ids: [genre.id], artist:) }

    context 'when is a success' do
      it 'updates a story' do
        sign_in user

        put "/stories/#{story.id}", params: { story: { title: 'new title' } }

        expect(response).to redirect_to(story_path(story.id)) 
      end
    end

    context 'when is a failure' do
      it 'does not update a story' do
        sign_in user

        put "/stories/#{story.id}", params: { story: { title: '' } }

        expect(response).to render_template(:edit)        
      end
    end

    context 'when a user contribute to a story' do
      it 'create a contributor' do
        sign_in user_two

        put "/stories/#{story.id}", params: { story: { body: 'new body' } }

        expect(Contribution.first.user_id).to eq user_two.id       
      end
    end
  end
end
