require 'rails_helper'

RSpec.describe "Likes", type: :request do
  describe "POST /create" do
    let(:user)  { Fabricate(:user) }
    let(:era) { Fabricate(:era, year: '2000') }
    let(:genre) { Fabricate(:genre, name: 'jazz') }
    let(:artist) { Fabricate(:artist) }
    let(:story) { Fabricate(:story, user_id: user.id, era_id: era.id, genre_ids: [genre.id], artist:) }

    before do
      sign_in user
    end

    it 'creates a like' do
      story

      expect do
        post '/likes', params: { like: { user_id: user.id, story_id: story.id } }
      end.to change(Like, :count).by(1)
    end
  end

  describe 'DELETE /destroy' do
    let(:user)  { Fabricate(:user) }
    let(:like)  { Fabricate(:like, user_id: user.id, story_id: story.id) }
    let(:era) { Fabricate(:era, year: '2000') }
    let(:genre) { Fabricate(:genre, name: 'jazz') }
    let(:artist) { Fabricate(:artist) }
    let(:story) { Fabricate(:story, user_id: user.id, era_id: era.id, genre_ids: [genre.id], artist:) }

    before do
      sign_in user
    end

    it 'deletes like' do
      story
      like

      delete "/likes/#{like.id}", params: { like: { story_id: like.story_id } }

      expect(Like.count).to eq 0
    end
  end
end
