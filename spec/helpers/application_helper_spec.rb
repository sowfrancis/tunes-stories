require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'model_avatar' do
    let(:user) { Fabricate(:user) }

    it 'check avatar is present' do
      expect(helper.model_avatar(user, 'rounded-circle avatar')).to have_css('.avatar')
    end
  end
end
