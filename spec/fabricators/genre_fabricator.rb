Fabricator(:genre) do
  name { FFaker::Music.genre }
end
