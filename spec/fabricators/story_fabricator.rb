# frozen_string_literal: true

Fabricator(:story) do
  title { FFaker::Book.title }
  artist { Fabricate(:artist, name: FFaker::Music.artist) }
  body { FFaker::Book.description }
  user { Fabricate(:user) }
  era { Fabricate(:era, year: '1990') }
  genres { [Fabricate(:genre, name: 'Jazz')] }
end
