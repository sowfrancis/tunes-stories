require 'rails_helper'

RSpec.describe SendNotificationsService, type: :model do
  describe '#call' do
    context 'when user contribute to story' do
      let(:user) { Fabricate(:user) }
      let(:story) { Fabricate(:story) }
      let(:object) { Fabricate(:contribution, user_id: user.id, story_id: story.id) }

      it 'sends a notification to user' do
        described_class.new(object, user, story).call

        expect(Notification.first.user.id).to eq story.user.id
      end
    end

    context 'when user comment a story' do
      let(:user) { Fabricate(:user) }
      let(:story) { Fabricate(:story) }
      let(:object) { Fabricate(:comment, body: 'text', user_id: user.id, story_id: story.id) }

      it 'sends a notification to user' do
        described_class.new(object, user, story).call

        expect(Notification.first.user.id).to eq story.user.id
      end
    end
  end
end
