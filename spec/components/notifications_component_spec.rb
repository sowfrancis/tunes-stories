# frozen_string_literal: true

require "rails_helper"

RSpec.describe NotificationsComponent, type: :component do
  describe 'notice' do
    let(:user) { Fabricate(:user) }
    let(:notice) { 'Vous êtes connecté(e).' }
    let(:alert)  { 'Vous vous êtes déconnecté(e).' }

    it "renders notice" do
      render_inline(described_class.new(notice:, alert:)) { notice.to_s }

      expect(page).to have_text notice
    end

    it 'renders alert' do
      render_inline(described_class.new(notice:, alert:)) { alert.to_s }

      expect(page).to have_text notice
    end
  end
end
