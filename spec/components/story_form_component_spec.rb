# frozen_string_literal: true

require "rails_helper"

RSpec.describe StoryFormComponent, type: :component do
  describe 'new story form' do
    let(:user) { Fabricate(:user) }
    let(:era) { Fabricate(:era, year: '2000') }
    let(:genre) { Fabricate(:genre, name: 'Hip hop') }

    it "renders new story form" do
      story = Fabricate.build(:story, user:, era:, genre_ids: [genre.id])

      render_inline(described_class.new(story:)) { story }

      expect(page).to have_text 'Titre'
    end
  end
end
