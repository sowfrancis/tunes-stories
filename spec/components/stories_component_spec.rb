# frozen_string_literal: true

require "rails_helper"

RSpec.describe StoriesComponent, type: :component do
  describe 'stories' do
    let(:user) { Fabricate(:user) }
    let(:era) { Fabricate(:era, year: '2000') }
    let(:genre) { Fabricate(:genre, name: 'Hip hop') }
    let(:artist) { Fabricate(:artist) }
    let(:stories) { Fabricate.times(2, :story, user:, era:, genre_ids: [genre.id], artist:) }

    it "renders display all stories" do
      render_inline(described_class.new(stories:)) { stories }

      expect(page).to have_text stories.first.artist.name.capitalize
    end
  end
end
