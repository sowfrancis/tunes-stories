# frozen_string_literal: true

require "rails_helper"

RSpec.describe NavbarComponent, type: :component do
  it "renders something useful" do
    render_inline(described_class.new(navbar: "navabar")) { 'Navbar' }

    expect(page).to have_text 'Home'
  end
end
