require 'rails_helper'

RSpec.describe Artist, type: :model do
  describe 'Association' do
    it { is_expected.to have_many(:stories) }
  end

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:name) }
  end

  describe 'callback' do
    describe 'before_create' do
      let(:artist) { Fabricate.build(:artist, name: 'J dilla') }

      it 'downcase the name' do
        artist.save

        expect(artist.name).to eq 'j dilla'
      end
    end
  end
end
