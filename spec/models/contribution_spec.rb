require 'rails_helper'

RSpec.describe Contribution, type: :model do
  describe 'Association' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:story) }
  end

  describe 'contribute' do
    let(:user) { Fabricate(:user) }
    let(:user_two) { Fabricate(:user) }
    let(:artist) { Fabricate(:artist, name: 'J cole') }
    let(:artist_two) { Fabricate(:artist, name: 'kendrick') }
    let(:genre) { Fabricate(:genre, name: 'jazz') }
    let(:genre_two) { Fabricate(:genre, name: 'hip hop') }
    let(:era) { Fabricate(:era, year: '1990') }
    let(:era_two) { Fabricate(:era, year: '2000') }
    let(:story) { Fabricate(:story, title: 'Forest', era:, genre_ids: [genre.id], user:, artist:) }
    let(:story_two) do
      Fabricate(:story, title: 'butterfly', era: era_two,
                        genre_ids: [genre_two.id], user: user_two, artist: artist_two)
    end

    context 'when user contribute to story' do
      it 'creates a contribution' do
        described_class.contribute(user, story_two)

        expect(described_class.count).to eq 1
      end
    end
  end
end
