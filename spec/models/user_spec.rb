require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'Association' do
    it { is_expected.to have_many(:stories) }
    it { is_expected.to have_many(:likes) }
    it { is_expected.to have_many(:notifications) }
    it { is_expected.to have_many(:comments) }
  end
end
