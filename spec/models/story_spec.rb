require 'rails_helper'

RSpec.describe Story, type: :model do
  describe 'Association' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:era) }
    it { is_expected.to belong_to(:artist) }
    it { is_expected.to have_many(:story_genres) }
    it { is_expected.to have_many(:genres) }
    it { is_expected.to have_many(:contributions) }
    it { is_expected.to have_many(:users) }
    it { is_expected.to accept_nested_attributes_for :artist }
  end

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:body) }
  end
end
