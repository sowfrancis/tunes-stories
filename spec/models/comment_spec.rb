require 'rails_helper'

RSpec.describe Comment, type: :model do
  describe 'Association' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:story) }
  end

  describe 'Validations' do
    it { is_expected.to validate_presence_of(:body) }
  end
end
