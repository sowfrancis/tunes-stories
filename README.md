## Name
Tunes-stories

## Description

Application participatif permettant à une communauté de partager les histoires de processus de création d'albums, morceaux de leur artistes préférés.

## Visuals

![sign_in](app/assets/images/visual/sign_in.png)

![sign_up](app/assets/images/visual/Capture d’écran du 2023-11-29 16-39-25.png)

![stories](app/assets/images/visual/Capture d’écran du 2024-01-10 15-51-47.png)

![story show](app/assets/images/visual/Capture d’écran du 2023-12-07 18-36-36.png)


## Installation

- install ruby 3.0.0 because the application is on rails 7 

- install postgresql 14 for the database

- install imageMagick for the image_processing gem to work

- bundle install

- rails db:create for creating the database development and test

- rake db:seed to create data

- rspec to launch the test suite

- rails s

* Deployment instructions

* ...

