class SearchStories < ApplicationService
  def initialize(stories, search_input)
    @stories = stories
    @search_input = search_input
  end

  def call
    return @stories.find_by_sql(search_sql) if @search_input.present?

    @stories.includes(:artist).order(created_at: 'DESC')
  end

  def search_sql
    result = find_by_era if @search_input['era'].present?
    result = find_by_genres if @search_input['genres'].present?
    result = find_by_queries if @search_input['queries'].present?

    result
  end

  def find_by_era
    "SELECT * from stories
     INNER JOIN eras ON stories.era_id = eras.id
     WHERE eras.year = '#{@search_input['era']}'
    "
  end

  def find_by_genres
    "SELECT * FROM stories
     INNER JOIN story_genres ON stories.id = story_genres.story_id
     INNER JOIN genres ON genres.id = story_genres.genre_id
     WHERE genres.name = '#{@search_input['genres'].downcase}'
    "
  end

  def find_by_queries
    "SELECT * FROM stories
     INNER JOIN artists ON stories.artist_id = artists.id
     WHERE artists.name = '#{@search_input['queries'].downcase}' OR stories.title = '#{@search_input['queries'].downcase}'
    "
  end
end
