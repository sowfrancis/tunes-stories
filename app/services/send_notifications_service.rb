class SendNotificationsService < ApplicationService
  def initialize(object, user, story)
    @object = object
    @user = user
    @story = story
  end

  def call
    send_notification
  end

  def send_notification
    Notification.create(user_id: @story.user.id, story_id: @story.id, content:)
  end

  def content
    case @object
    when @object.instance_of?(::Contribution)
      I18n.t('activerecord.contribution.notification.content',
             user_fullname: @object.user.fullname, title: @story.title)
    when @object.instance_of?(::Comment)
      I18n.t('activerecord.comment.notification.content',
             user_fullname: @object.user.fullname, title: @object.story.title)
    end
  end
end
