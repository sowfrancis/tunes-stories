// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "jquery"
import "popper"
import * as bootstrap from "bootstrap"
import "@hotwired/turbo-rails"
import "controllers"
import "trix"
import "@rails/actiontext"
import "select2"

$(document).ready(function() {
  $('#genres-select').select2({
    theme: "bootstrap"
  });

  $('#genres-filters').select2({
    theme: "bootstrap",
    placeholder: 'Genres'
  });

  setTimeout(function() {
    $('.paper').fadeOut();
  }, 5000);

  $('.toast').toast('show');
});
