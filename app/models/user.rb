class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable

  has_many :stories
  has_many :likes
  has_many :stories, through: :likes
  has_many :notifications
  has_many :stories, through: :notifications
  has_one_attached :avatar
  has_many :comments

  before_create :skip_user_confirmation

  def skip_user_confirmation
    skip_confirmation!
  end

  def fullname
    "#{first_name} #{last_name}"
  end

  def unviewed_notifications_count
    notifications.where(viewed: false).count
  end
end
