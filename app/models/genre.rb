class Genre < ApplicationRecord
	has_many :story_genres
	has_many :stories, through: :story_genres

	validates :name, presence: true

	before_create do
		self.name = name.downcase
	end
end
