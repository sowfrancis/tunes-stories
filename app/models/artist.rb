class Artist < ApplicationRecord
  has_many :stories
  has_one_attached :avatar

  before_create do
    self.name = name.downcase
  end

  validates :name, presence: true
end
