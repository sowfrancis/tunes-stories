class Story < ApplicationRecord
  belongs_to :user
  belongs_to :era
  belongs_to :artist
  has_many :story_genres
  has_many :genres, through: :story_genres
  has_many :contributions
  has_many :users, through: :contributions
  has_many :likes
  has_many :users, through: :likes
  has_many :notifications
  has_many :users, through: :notifications
  has_many :comments

  accepts_nested_attributes_for :artist

  has_rich_text :body
  has_one_attached :avatar

  validates :title, :body, presence: true

  before_create do
    self.title = title.downcase
  end
  before_create :existing_artist

  def existing_artist
    artist = Artist.find_by(name: self.artist.name)

    self.artist = artist
  end

  def contributors_count
    contributions.count
  end
end
