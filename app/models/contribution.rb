class Contribution < ApplicationRecord
  belongs_to :story
  belongs_to :user

  def self.contribute(contributor, story)
    return if contributor.id == story.user.id

    find_or_create_by(user_id: contributor.id, story_id: story.id)
  end
end
