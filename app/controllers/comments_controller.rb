class CommentsController < ApplicationController
  before_action :authenticate_user!, only: %i[create]
  before_action :find_story, only: %i[new create]

  def create
    @comment = @story.comments.new(comment_params)
    @comment.user_id = current_user.id

    if @comment.save
      SendNotificationsService.new(@comment, current_user, @story).call unless @story.user_id == current_user.id
    else
      redirect_to story_path(@story), notice: "Un problème est survenue"
    end
  end

  private

  def find_story
    @story = Story.find(params[:story_id])
  end

  def comment_params
    params.require(:comment).permit(:body, :user_id, :story_id)
  end
end
