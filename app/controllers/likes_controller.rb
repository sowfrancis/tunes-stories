class LikesController < ApplicationController
  before_action :story

  def create
    Like.create(user_id: current_user.id, story_id: @story.id)
    redirect_to story_path(@story)
  end

  def destroy
    @like = Like.find(params[:id])

    @like.destroy
    redirect_to story_path(@story)
  end

  private

  def story
    @story = Story.find(params[:like][:story_id])
  end

  def like_params
    params.require(:like).permit(:user_id, :story_id)
  end
end
