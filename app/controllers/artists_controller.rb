class ArtistsController < ApplicationController
  before_action :artist, only: [:show]

  def show
    @stories = @artist.stories
  end

  private

  def artist
    @artist = Artist.find(params[:id])
  end
end
