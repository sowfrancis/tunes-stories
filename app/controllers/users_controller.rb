class UsersController < ApplicationController
  before_action :find_user, only: [:show, :index, :edit]

  def show
    @stories = @user.stories
  end

  def edit; end

  private

  def find_user
    @user = User.includes(:stories).find(params[:id])
  end
end
