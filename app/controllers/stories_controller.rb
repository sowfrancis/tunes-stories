# frozen_string_literal: true

class StoriesController < ApplicationController
  before_action :authenticate_user!, only: %i[new create]
  before_action :find_story, only: %i[show edit update destroy]
  before_action :find_like, only: %i[show edit update destroy]

  def index
    @stories = SearchStories.call(Story.all, params[:q])
  end

  def new
    @story = Story.new
    @artist = @story.build_artist
  end

  def create
    @story = current_user.stories.new(story_params)
    @story.user_id = current_user.id

    if @story.save
      redirect_to story_path(@story), notice: t('activerecord.controllers.stories.create.success')
    else
      render 'new'
    end
  end

  def show
    @comment = Comment.new
    @comments = @story.comments
  end

  def edit; end

  def update
    if @story.update(story_params)
      SendNotificationsService.new(Contribution.contribute(current_user, @story), current_user, @story).call
      redirect_to story_path(@story), notice: t('activerecord.controllers.stories.update.success')
    else
      render 'edit'
    end
  end

  private

  def find_story
    @story = Story.find(params[:id])
  end

  def find_like
    @like = @story.likes.find_by(user_id: current_user.id, story_id: @story.id) if current_user
  end

  def story_params
    params.require(:story).permit(:title, :artist_name, :body, :user_id, :era_id, :avatar,
                                  artist_attributes: [:id, :name, :avatar], genre_ids: [])
  end
end
