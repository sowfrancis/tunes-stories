class NotificationsController < ApplicationController
  before_action :authenticate_user!, only: %i[index]

  def index
    @notifications = current_user.notifications.order(created_at: 'DESC')
    update_notifications(@notifications)
  end

  private

  def update_notifications(notifications)
    notifications.each do |notification|
      next if notification.viewed == true

      notification.update(viewed: true)
    end
  end
end
