module StoriesHelper
  def story_title(title)
    %("#{title}")
  end

  def story_form_title(story)
    if story.new_record?
      'Nouvelle histoire'
    else
      'Editer votre histoire'
    end
  end

  def edit_button_label(story)
    return 'Contribuer' unless story.user.id == current_user.id

    'Editer votre histoire'
  end
end
