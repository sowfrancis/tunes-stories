module ApplicationHelper
  def model_avatar(model, css)
    if model.avatar.url
      image_tag(model.avatar.url, class: css)
    else
      image_tag('empty-avatar.png', class: css)
    end
  end
end
