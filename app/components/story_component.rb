# frozen_string_literal: true

class StoryComponent < ViewComponent::Base
  include StoriesHelper

  def initialize(story:)
    @story = story
  end
end
