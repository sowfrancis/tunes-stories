# frozen_string_literal: true

class NotificationsCountComponent < ViewComponent::Base
  include Turbo::StreamsHelper

  def initialize(current_user:)
    @current_user = current_user
  end

  def user_unviewed_notifications_count
    @current_user.unviewed_notifications_count
  end
end
