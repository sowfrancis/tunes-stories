# frozen_string_literal: true

class StoriesComponent < ViewComponent::Base
  include StoriesHelper

  def initialize(stories:)
    @stories = stories
  end
end
