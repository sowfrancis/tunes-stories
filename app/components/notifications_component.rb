# frozen_string_literal: true

class NotificationsComponent < ViewComponent::Base
  include ApplicationHelper

  def initialize(notice:, alert:)
    @notice = notice
    @alert = alert
  end
end
