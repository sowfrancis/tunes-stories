# frozen_string_literal: true

class CommentsListComponent < ViewComponent::Base
  def initialize(comments:)
    @comments = comments
  end

  def created_at_format(created_at)
    created_at.strftime("%d/%m/%Y, %H:%M:%S")
  end
end
