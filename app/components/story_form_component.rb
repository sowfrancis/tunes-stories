# frozen_string_literal: true

class StoryFormComponent < ViewComponent::Base
	delegate :rich_text_area_tag, to: :helpers
	include StoriesHelper

	def initialize(story:)
		@story = story
	end
end
