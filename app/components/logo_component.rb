# frozen_string_literal: true

class LogoComponent < ViewComponent::Base
  def initialize(logo:)
    @logo = logo
  end
end
