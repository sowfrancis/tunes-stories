# frozen_string_literal: true

class CommentFormComponent < ViewComponent::Base
  def initialize(comment:, story:)
    @comment = comment
    @story = story
  end
end
