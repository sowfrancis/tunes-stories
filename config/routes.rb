Rails.application.routes.draw do
  get 'house/index'
  get 'house/edit'
  devise_for :users, controllers: { registrations: "users/registrations" }
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "stories#index"

  resources :users, only: [:show, :index, :edit]
  resources :stories, only: [:create, :new, :index, :show, :edit, :update] do
    resources :comments, only: [:create]
  end
  resources :artists, only: [:show]
  resources :likes, only: [:create, :destroy]
  resources :notifications, only: [:index]
end
